var http = require('http');

const { SerialPort } = require('serialport')
const { ReadlineParser } = require('@serialport/parser-readline')
const port = new SerialPort({ path: '/dev/ttyACM0', baudRate: 9600 })

const parser = port.pipe(new ReadlineParser({ delimiter: '\r\n' }))

const express = require('express');
const app = express();
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.use(express.static('public'));

io.on('connection', (socket) => {
  // console.log('a user connected');
});

server.listen(3000, () => {
  // console.log('listening on *:3000');
});

parser.on('data', function(data) {
  console.log('Received data from port: ' + data);
  io.emit('data', data);
});