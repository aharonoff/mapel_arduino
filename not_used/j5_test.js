var five = require("johnny-five");
var board = new five.Board();

var toggles = [0, 0, 0, 0, 0];
board.on("ready", function() {
  var toggle_1 = new five.Switch(4);
  var toggle_2 = new five.Switch(6);
  var toggle_3 = new five.Switch(8);
  var toggle_4 = new five.Switch(10);
  var toggle_5 = new five.Switch(12);
  board.repl.inject({
    toggle: toggle_1,
    toggle_2,
    toggle_3,
    toggle_4,
    toggle_5
  });

  toggle_1.on("close", function() {
    toggles[0] = 1;
  });
  toggle_1.on("open", function() {
    toggles[0] = 0;
  });
  toggle_2.on("close", function() {
    toggles[1] = 1;
  });
  toggle_2.on("open", function() {
    toggles[1] = 0;
  });
  toggle_3.on("close", function() {
    toggles[2] = 1;
  });
  toggle_3.on("open", function() {
    toggles[2] = 0;
  });
  toggle_4.on("close", function() {
    toggles[3] = 1;
  });
  toggle_4.on("open", function() {
    toggles[3] = 0;
  });
  toggle_5.on("close", function() {
    toggles[4] = 1;
  });
  toggle_5.on("open", function() {
    toggles[4] = 0;
  });

  this.loop(5000, () => {
    console.log(toggles);
  });
});