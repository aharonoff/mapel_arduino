const int REED_PIN_1 = 4;
const int REED_PIN_2 = 6;
const int REED_PIN_3 = 8;
const int REED_PIN_4 = 10;
const int REED_PIN_5 = 12;

int PIN_STATES[] = {1, 1, 1, 1, 1};

void setup() 
{
  Serial.begin(9600);
  pinMode(REED_PIN_1, INPUT_PULLUP);
  pinMode(REED_PIN_2, INPUT_PULLUP);
  pinMode(REED_PIN_3, INPUT_PULLUP);
  pinMode(REED_PIN_4, INPUT_PULLUP);
  pinMode(REED_PIN_5, INPUT_PULLUP);
}

void loop() 
{
  int proximity_1 = digitalRead(REED_PIN_1);
  if (proximity_1 == LOW)
  {
    PIN_STATES[0] = 0;
    Serial.println("Reed 1 closed");
  }
  else
  {
    PIN_STATES[0] = 1;
  }

  int proximity_2 = digitalRead(REED_PIN_2);
  if (proximity_2 == LOW)
  {
    PIN_STATES[1] = 0;
    Serial.println("Reed 2 closed");
  }
  else
  {
    PIN_STATES[1] = 1;
  }

  int proximity_3 = digitalRead(REED_PIN_3);
  if (proximity_3 == LOW)
  {
    PIN_STATES[2] = 0;
    Serial.println("Reed 3 closed");
  }
  else
  {
    PIN_STATES[2] = 1;
  }

  int proximity_4 = digitalRead(REED_PIN_4);
  if (proximity_4 == LOW)
  {
    PIN_STATES[3] = 0;
    Serial.println("Reed 4 closed");
  }
  else
  {
    PIN_STATES[3] = 1;
  }

  int proximity_5 = digitalRead(REED_PIN_5);
  if (proximity_5 == LOW)
  {
    PIN_STATES[4] = 0;
    Serial.println("Reed 5 closed");
  }
  else
  {
    PIN_STATES[4] = 1;
  }

  int s = 0;
  for (int i = 0; i < sizeof(PIN_STATES); i++)
  {
    s += PIN_STATES[i];
  }
  if (PIN_STATES[0] + PIN_STATES[1] + PIN_STATES[2] + PIN_STATES[3] + PIN_STATES[4] == 5) {
    Serial.println("No level is activated.");
  } 
  // Serial.print(PIN_STATES[0]);
  // Serial.print(PIN_STATES[1]);
  // Serial.print(PIN_STATES[2]);
  // Serial.print(PIN_STATES[3]);
  // Serial.print(PIN_STATES[4]);
}