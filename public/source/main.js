let states = {
  editor: true,
  game: false
}
let reed_state = 0
let font

function preload() {
	// font = loadFont('fonts/Stupid Meeting_D.otf')
	// font = loadFont('fonts/CardboardCat-Bold.otf')
	// font = loadFont('fonts/DK Astromonkey.otf')
	// font = loadFont('fonts/Silverkids.otf')
	// font = loadFont('fonts/Square Squads.otf')
	// font = loadFont('fonts/JunkDog.otf')

	font = loadFont('fonts/Foulmouth.otf')
	// font = loadFont('fonts/Foulmouth Italic.otf')
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(RGB, 255, 255, 255, 1)
  textAlign(CENTER, CENTER)
}

function draw() {
  background(0)
  noStroke()
  if (reed_state == 0) {
    fill(128)
  }
  if (reed_state == 1) {
    fill(255)
  }
  if (reed_state == 2) {
    fill(255, 255, 0)
  }
  if (reed_state == 3) {
    fill(255, 128, 0)
  }
  if (reed_state == 4) {
    fill(255, 0, 0)
  }
  if (reed_state == 5) {
    fill(0)
  }
  rect(0, 0, windowWidth, windowHeight)

  fill(128)
  noStroke()
  textFont(font)
  textSize(128)
  text('level ' + reed_state, 200, 64)
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight)
}

function keyPressed() {
  if (keyCode === 13) {
    console.log(JSON.stringify(states))
  }
}